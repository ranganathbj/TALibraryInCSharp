module.exports = {
    platform: 'gitlab',
    baseBranches: ['main','feature1','master'],
    labels: ['renovate'],
    rebaseWhen: 'auto',
    semanticCommits: true,
    major: {
      automerge: false
    },
    repositories: [
      'TALibraryInCSharp'
    ],
    extends: [
      'config:base',
      ":pinAllExceptPeerDependencies"
    ]
  };
